package com.example.goodnight;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.goodnight.databinding.FragmentMainBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends LifecycleFragment {

    private ArrayList<Bebida> items;
    private BebidasAdapter adapter;
    private FragmentMainBinding binding;
    private SharedPreferences preferences;
    private BebidasViewModel model;


    public MainActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ListView lvBebidas = (ListView) view.findViewById(R.id.lvBebidas);

        items = new ArrayList<>();
        adapter = new BebidasAdapter(
                getContext(),
                R.layout.lv_bebidas_row,
                items
        );
        lvBebidas.setAdapter(adapter);

        lvBebidas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    Bebida bebida = (Bebida) adapterView.getItemAtPosition(i);
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("Bebida", bebida);

                    startActivity(intent);
            }
        });
        model = ViewModelProviders.of(this).get(BebidasViewModel.class);
        model.getBebidas().observe(this, new Observer<List<Bebida>>() {
            @Override
            public void onChanged(@Nullable List<Bebida> bebidas) {
                adapter.clear();
                adapter.addAll(bebidas);
            }
        });


        return view;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_bebidas_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        if (id == R.id.action_refresh){
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void refresh(){

        model.reload();
    }

}
