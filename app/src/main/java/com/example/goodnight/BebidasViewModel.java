package com.example.goodnight;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lluc on 12/01/2018.
 */

public class BebidasViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final BebidasDAO bebidasDao;
    private static final int PAGES = 10;


    public BebidasViewModel(Application application) {
        super(application);
        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.bebidasDao = appDatabase.getBebidasDao();
    }

    public LiveData<List<Bebida>> getBebidas() {

       return bebidasDao.getBebidas();
    }

    public void reload() {

        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    private class RefreshDataTask extends AsyncTask<Void,Void, ArrayList<Bebida>>{
        @Override
        protected ArrayList<Bebida> doInBackground(Void... voids) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );

            String category = preferences.getString("primary_category", "Beer");
            String tipusConsulta = preferences.getString("Category", "Category");
            BebidasAPI api = new BebidasAPI();
            ArrayList<Bebida> result;

            if (tipusConsulta.equals("Category")){
                result = api.getBebidas(category);
            } else {
                result= api.getBebidas(category);
            }
                bebidasDao.deleteBebidas();
                bebidasDao.addBebidas(result);
            return result;
        }

    }
}
