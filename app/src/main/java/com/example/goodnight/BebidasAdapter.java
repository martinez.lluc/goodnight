package com.example.goodnight;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.goodnight.databinding.LvBebidasRowBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lluc on 09/01/2018.
 */

public class BebidasAdapter extends ArrayAdapter<Bebida>{

    public BebidasAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Bebida> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Bebida bebida = getItem(position);

        LvBebidasRowBinding binding = null;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.lv_bebidas_row, parent, false);
        } else {
            binding = DataBindingUtil.getBinding(convertView);
        }


        binding.tvName.setText(bebida.getName());
        Glide.with(getContext()).load(
                bebida.getImgDrink()).into(binding.ivBebidaImage);

        return binding.getRoot();
    }
}

