package com.example.goodnight;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.goodnight.databinding.FragmentDetailBinding;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    private TextView tvName;
    private View view;
    private ImageView ivBebida;
    private TextView txtCategory;

    private FragmentDetailBinding binding;

    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding= FragmentDetailBinding.inflate(inflater);
        view = binding.getRoot();
        Intent i = getActivity().getIntent();
        if (i != null){
            Bebida bebida = (Bebida) i.getSerializableExtra("Bebida");

            if (bebida!=null){
                updateUi(bebida);
            }
        }
        return view;
    }

    private void updateUi(Bebida bebida) {

        binding.tvName.setText(bebida.getName()+"/nCategory: "+bebida.getCategory());

        Glide.with(getContext()).load(bebida.getImgDrink()).into(binding.ivBebidaImage);
    }

}
