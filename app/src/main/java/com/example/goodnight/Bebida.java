package com.example.goodnight;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

/**
 * Created by Lluc on 04/01/2018.
 */
@Entity
public class Bebida implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String Category;
    private String imgDrink;

    @Override
    public String toString() {
        return "Bebida{" +
                "name='" + name + '\'' +
                ", Category='" + Category + '\'' +
                ", imgDrink='" + imgDrink + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }

    public String getImgDrink() {
        return imgDrink;
    }

    public void setImgDrink(String imgDrink) {
        this.imgDrink = imgDrink;
    }
}
