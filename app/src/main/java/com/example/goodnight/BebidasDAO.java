package com.example.goodnight;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Lluc on 12/01/2018.
 */
@Dao
public interface BebidasDAO {

     @Query("select * from bebida")
    LiveData<List<Bebida>> getBebidas();

     @Insert
    void addBebida(Bebida bebida);

     @Insert
     void addBebidas(List<Bebida> Bebidas);


     @Delete
    void deleteBebida (Bebida bebida);

     @Query("DELETE FROM bebida")
    void deleteBebidas();

}
